var ctx; //The canvas
var image; //The image
var imageData; //Image data
var circle; //The base circle (The rest are recursive children)
var timer;
$(document).ready(function(){
	var c = $('#imageCanvas')[0];
	ctx = c.getContext("2d");
	image = new Image();
	image.onload = function(){
		initImage();
		ctx.clearRect(0,0,500,500);
		
		circle = makeCircle();
		circle.init(0,0,500,500);
		circle.getColour();
		circle.draw();
	}
	image.src="flower.jpg";
	$(document).on('mousemove', function(evt)
	{
		$('#outputDiv').html('<h5>['+(evt.pageX-$('#imageCanvas').offset().left).toFixed(0) + ','+(evt.pageY-$('#imageCanvas').offset().top).toFixed(0)+']</h5>');
		circle.processMousePos((evt.pageX-$('#imageCanvas').offset().left).toFixed(0),(evt.pageY-$('#imageCanvas').offset().top).toFixed(0));
	});
	$('#doRandom').on('click', function()
	{
		window.clearTimeout();
		doRandom();
	});
});

doRandom = function()
{
	circle.randomMakeChildren(); 
	window.setTimeout(doRandom, 50);	
}

initImage = function()
{
	ctx.drawImage(image, 0,0);
	imageData = ctx.getImageData(0,0,500,500);
}

makeCircle = function()
{
	var newCircle = {};
	newCircle.dim={};
	newCircle.circle={};
	newCircle.colour;
	newCircle.children = [];
	newCircle.tooSmall = false;
	newCircle.init = function(x,y,width,height)
	{
		newCircle.dim.x=x;
		newCircle.dim.y=y;
		newCircle.dim.width=width;
		newCircle.dim.height=height;
		newCircle.circle.x = newCircle.dim.x + newCircle.dim.width/2;
		newCircle.circle.y = newCircle.dim.y + newCircle.dim.height/2;
		newCircle.circle.rad = newCircle.dim.width/2;
		if (newCircle.circle.rad < 3)
			newCircle.tooSmall = true;
	}
	//Checks if the mouse is in the square that this circle occupies, there is no need to do the more intensive radius check if it isn't in the square
	newCircle.posEnclosed = function(x,y)
	{
		return (x > newCircle.dim.x && x < newCircle.dim.x + newCircle.dim.width && y > newCircle.dim.y && y < newCircle.dim.y + newCircle.dim.height);
	}
	//Creates the 4 recursive children when this circle is moused over
	newCircle.makeChildren = function()
	{
		ctx.clearRect(newCircle.dim.x, newCircle.dim.y, newCircle.dim.width, newCircle.dim.height);
		for (var i = 0; i < 2; i++)
			for (var j = 0; j < 2; j++)
			{				
				var temp = makeCircle();
				temp.init(newCircle.dim.x + i*newCircle.dim.width/2, newCircle.dim.y + j*newCircle.dim.height/2, newCircle.dim.width/2, newCircle.dim.height/2);
				temp.getColour();
				temp.draw();
				newCircle.children.push(temp);				
			}
	}
	//This should find the average colour in the square that this specific circle occupies on the canvas
	newCircle.getColour = function()
	{
		var col = [0,0,0];
		var count = 0;
		for (var i = Math.round(newCircle.dim.x); i< Math.ceil(newCircle.dim.x + newCircle.dim.width); i++)
			for (var j = Math.round(newCircle.dim.y); j < Math.ceil(newCircle.dim.y + newCircle.dim.height); j++)
			{
				count++;
				for (var c = 0; c < 3; c++)
				{
					col[c] += imageData.data[(i + j * 500)*4+c];
				}
			}
			
		for (var i = 0; i < 3; i++)
		{
			col[i] = Math.round(col[i]/count)
			if (col[i] > 255)
				col[i] = 255;
			col[i] = col[i].toString(16);
			if (col[i].length < 2)
				col[i] = '0'+col[i];
		}
		
		newCircle.colour = '#'+col[0].toString(16) + col[1].toString(16) + col[2].toString(16);
		//console.log(newCircle.colour);
	}
	//Processes the mouse position
	newCircle.processMousePos = function(x,y)
	{
		if (newCircle.tooSmall)
			return;
		//No need to process if the mouse is nowhere near
		if (!newCircle.posEnclosed(x,y))
			return;
		
		//If this circle has children, they need to check the mouse position relative to themselves, so the childrens' processMousePos() methods are called
		if (newCircle.children.length > 0){
			for(var i = 0; i < newCircle.children.length; i++)
				newCircle.children[i].processMousePos(x,y);
		}
		//For a circle with no children - the recursive base case
		else
		{
			var dist = Math.sqrt(Math.pow((newCircle.circle.x - x),2) + Math.pow((newCircle.circle.y - y),2));
			if (dist < newCircle.circle.rad)
			{
				newCircle.makeChildren();
			}
		}
	}
	newCircle.draw = function()
	{
		//The circle is only drawn if it has no children, since if it has children, they should be drawn instead
		ctx.fillStyle = newCircle.colour;
		ctx.beginPath();
		ctx.arc(newCircle.circle.x, newCircle.circle.y, newCircle.circle.rad, 0, Math.PI * 2)
		ctx.fill();
		ctx.closePath();
	}
	newCircle.randomMakeChildren = function()
	{
		if (newCircle.tooSmall)
			return true;
		if (newCircle.children.length > 0)for (var i = 0; i < 4; i++)
		{
			if (newCircle.children[i].randomMakeChildren())
				break;
		}else{
			if (Math.random() > 0.9){
				newCircle.makeChildren();
				return true;
			}
		}
		return false;
	}
	return newCircle;
}